# README #

### What is this repository for? ###
WooliesX Coding exam

* Quick summary
Canine Breed List, using UItableView 
The list can be sorting by Ascending and Descending 

* Version 1.0

### How do I get set up? ###
* Dependencies
 - Alamofire
 - RXSwift

* Writing tests
Included in the project using XCTest

### Future Enhancement ###
- Retry when API fail to load
- Better UI Design
- Error Alert for API Failure